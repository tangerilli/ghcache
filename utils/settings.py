# coding=utf-8
# It's important that this file doesn't import any Django modules (and especially
# settings) because it is meant to be imported by settings files.

from __future__ import absolute_import

import importlib
import os
import sys
from ConfigParser import RawConfigParser
from logging import getLogger

l = getLogger(__name__)


def import_settings_mixin():
    """
    If settings have been set with the `DJANGO_CONF` environment variable by, for
    example: `$ DJANGO_CONF=conf.settings.localproduction ./manage.py runserver`,
    then the settings specified in DJANGO_CONF will monkeypatch the settings into your
    settings.
    """
    mixin_module_name = os.environ.get('DJANGO_CONF', '')
    if mixin_module_name:
        current_settings_module = os.environ['DJANGO_SETTINGS_MODULE']
        current_settings = importlib.import_module(current_settings_module)
        mixin_module = importlib.import_module(mixin_module_name)

        for setting_name in dir(mixin_module):
            mixin_value = getattr(mixin_module, setting_name)
            setattr(current_settings, setting_name, mixin_value)


def get_config():
    config_parser = RawConfigParser()
    config_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'config.ini')
    config_parser.read(config_path)
    return config_parser


def get_settings_module(config_parser=None):
    """
    The settings module ultimately used will be determined in this priority (descending):

    1. The --settings flag (though that isn't part of this method) **
      - ex: ./manage.py runserver --settings=conf.settings.dev
    2. Your config file's django.settings_module setting
    3. If the DJANGO_SETTINGS_MODULE env variable has been set, use that
    4. The test settings will be used if 'test' exists in the call arguments
    5. The production settings will be used if no other module is provided
    """
    if config_parser is None:
        config_parser = get_config()
    if _config_option_is_defined('django', 'settings_module', config_parser):
        l.info("Using the settings module defined in your config file")
        return config_parser.get('django', 'settings_module')
    elif os.environ.get('DJANGO_SETTINGS_MODULE'):
        l.info("The environment variable for your settings module has been set directly")
        return os.environ.get('DJANGO_SETTINGS_MODULE')
    elif 'test' in sys.argv:
        l.info("Using conf.settings.test")
        return 'conf.settings.test'
    else:
        l.info("Using production settings by default")
        return 'ghcache.settings'


def setup_django(settings_module=None):
    if settings_module is None:
        settings_module = get_settings_module(get_config())
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)
    import django
    django.setup()


def _config_option_is_defined(section, item, config_parser):
    return config_parser.has_option(section, item) and config_parser.get(section, item)

