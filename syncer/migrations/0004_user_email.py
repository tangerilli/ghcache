# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2018-02-19 17:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('syncer', '0003_repository_github_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='email',
            field=models.CharField(default='', max_length=1024),
            preserve_default=False,
        ),
    ]
