from __future__ import absolute_import, unicode_literals, print_function

import datetime
import re

import dateutil
import requests
from django.conf import settings
from django.db import transaction
from requests.auth import HTTPBasicAuth

from syncer.models import Issue, State, Milestone, Label, Repository, User, ReviewerM2M, CommitterM2M, Commit

MINIMUM_HISTORY = 60
REPOSITORIES = ['7Geese', '7geese-chrome']

def _auth_get_request(url, **kwargs):
    return requests.get(url, auth=HTTPBasicAuth(settings.GH_TOKEN, 'x-oauth-basic'), **kwargs)


def _get_next_page(response):
    links = response.headers.get('link', '').split(',')
    for link in links:
        if 'rel="next"' in link:
            return link.split(';')[0].replace('<', '').replace('>', '')
    return None


@transaction.atomic
def update_issues(repo_names=None):
    if repo_names is None:
        repo_names = REPOSITORIES
    for repo_name in repo_names:
        repository, _ = Repository.objects.get_or_create(name=repo_name)
        last_updated_issue = Issue.objects.filter(repository=repository).order_by('-modified_at').first()
        if last_updated_issue is None:
            update_since = datetime.datetime.now() - datetime.timedelta(days=MINIMUM_HISTORY)
        else:
            update_since = last_updated_issue.modified_at

        issues = update_issues_since(update_since, repository)
        for issue in issues:
            sync_issue(repository, issue)


def update_issues_since(timestamp, repository):
    issues = []
    url = 'https://api.github.com/repos/7Geese/{}/issues?sort=updated&direction=desc&state=closed&since={}'.format(repository.name, timestamp.isoformat())
    while True:
        print('Fetching {}'.format(url))
        response = _auth_get_request(url)
        response_issues = response.json()
        for issue in response_issues:
            issues.append(issue)

        url = _get_next_page(response)
        if not url:
            break

    return issues


def update_issue(repository, issue_number):
    url = 'https://api.github.com/repos/7Geese/{}/issues/{}'.format(repository.name, issue_number)
    response = _auth_get_request(url)
    response.raise_for_status()
    return sync_issue(repository, response.json())

def update_committers(issue, pr_data):
    url = pr_data['commits_url']

    while True:
        response = _auth_get_request(url)
        response.raise_for_status()
        committers = set()
        issue.committers.clear()
        for commit in response.json():
            user = None
            date_str = None
            message = commit['commit']['message']
            # Skip the commits where the author just merged staging into the branch, since that's not really contributing to the branch
            if re.match(r'Merge.*?branch.*?staging.*?into', message):
                continue
            if 'author' in commit and commit['author'] and 'login' in commit['author']:
                user, _ = User.objects.get_or_create(username=commit['author']['login'])
                committers.add(user)
            elif 'committer' in commit['commit']:
                email = commit['commit']['committer'].get('email')
                if email:
                    try:
                        user = User.objects.get(email=email)
                        committers.add(user)
                    except User.DoesNotExist:
                        print('Could not find email {}'.format(email))

            if user:
                date_str = commit['commit']['committer']['date']
                created = dateutil.parser.parse(date_str)
                defaults = {
                    'author': user,
                    'message': message,
                    'created': created
                }
                Commit.objects.update_or_create(hash=commit['sha'], issue=issue, defaults=defaults)

        url = _get_next_page(response)
        if not url:
            break

    # TODO: Set the number of changes for each user
    for user in committers:
        CommitterM2M.objects.create(user=user, issue=issue)


def update_reviewers(issue, pr_data):
    url = pr_data['url'] + '/reviews'
    response = _auth_get_request(url, headers={'Accept': 'application/vnd.github.black-cat-preview+json'})
    response.raise_for_status()
    issue.reviewers.clear()
    review_users = {}
    for review in response.json():
        user, _ = User.objects.get_or_create(username=review['user']['login'])
        approved = review['state'].lower() == 'approved'
        review_users[user] = review_users.get(user, False) or approved

    # TODO: Get comments (https://developer.github.com/v3/pulls/comments/)

    for user, approved in review_users.items():
        ReviewerM2M.objects.create(user=user, issue=issue, approved=approved)


def get_points_from_zenhub(repository, issue_number):
    url = 'https://api.zenhub.io/p1/repositories/{}/issues/{}'.format(repository.github_id, issue_number)
    response = requests.get(url, headers={'X-Authentication-Token': settings.ZENHUB_TOKEN})
    response.raise_for_status()
    json = response.json()
    return json.get('estimate', {}).get('value')


@transaction.atomic
def sync_issue(repository, data):
    created_at = dateutil.parser.parse(data['created_at'])
    modified_at = dateutil.parser.parse(data['updated_at'])
    defaults = {
        'created_at': created_at,
        'modified_at': modified_at
    }
    issue, _ = Issue.objects.get_or_create(number=data['number'], repository=repository, defaults=defaults)
    issue.title = data['title']
    issue.state = State.open.value if data['state'] == 'open' else State.closed.value
    if data.get('milestone'):
        issue.milestone, _ = Milestone.objects.get_or_create(name=data['milestone']['title'])

    labels = [Label.objects.get_or_create(name=label_data['name'])[0] for label_data in data['labels']]
    issue.labels.set(labels)

    if data.get('pull_request'):
        response = _auth_get_request(data['pull_request']['url'])
        response.raise_for_status()
        pr_data = response.json()

        issue.pr_opened_at = dateutil.parser.parse(pr_data['created_at'])
        issue.lines_added = pr_data['additions']
        issue.lines_removed = pr_data['deletions']

        update_committers(issue, pr_data)
        update_reviewers(issue, pr_data)


    issue.created_at = created_at
    issue.modified_at = modified_at
    issue.closed_at = dateutil.parser.parse(data['closed_at'])
    issue.save()

    if issue.points is None:
        issue.points = get_points_from_zenhub(repository, issue.number)
        issue.save()

    return issue