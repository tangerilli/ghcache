from django.contrib import admin

from syncer.models import *

admin.site.register(Issue)
admin.site.register(Milestone)
admin.site.register(Label)
admin.site.register(ReviewerM2M)
admin.site.register(CommitterM2M)
admin.site.register(User)
