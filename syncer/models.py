from __future__ import unicode_literals

import re

import jsonfield
from django.db import models
from enum import Enum


# TODO: Only re-fetch issues that have been updated since we last fetched
# TODO: Figure out how to get lines changed per commit
# TODO: Make it use mysql or postgres
# TODO: Page that lists issues that are missing labels / points


POINT_PATTERN = re.compile("\(SP(\d+)\)")
MILESTONE_PATTERN = re.compile("Sprint (\d+)")


class State(Enum):
    open = 0
    closed = 1


class Milestone(models.Model):
    name = models.CharField(max_length=4096)
    sprint = models.IntegerField(null=True, db_index=True)

    def __str__(self):
        return '{} ({})'.format(self.name, self.sprint)

    def save(self, **kwargs):
        match = MILESTONE_PATTERN.search(self.name)
        if match:
            self.sprint = int(match.groups(0)[0])

        super(Milestone, self).save(**kwargs)


class Repository(models.Model):
    name = models.CharField(max_length=1024)
    github_id = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Label(models.Model):
    name = models.CharField(max_length=1024)

    def __str__(self):
        return self.name


class User(models.Model):
    username = models.CharField(max_length=1024)
    email = models.CharField(max_length=1024)

    def __str__(self):
        return self.username


class CommitterM2M(models.Model):
    user = models.ForeignKey(User)
    issue = models.ForeignKey('syncer.Issue')
    lines_changed = models.IntegerField(default=0)
    def __str__(self):
        return '{} committed to {}'.format(self.user, self.issue)


class Commit(models.Model):
    hash = models.CharField(max_length=256, db_index=True)
    author = models.ForeignKey(User)
    issue = models.ForeignKey('syncer.Issue')
    message = models.CharField(max_length=4096)
    created = models.DateTimeField()


class ReviewerM2M(models.Model):
    user = models.ForeignKey(User)
    issue = models.ForeignKey('syncer.Issue')
    approved = models.BooleanField(default=False)
    comment_count = models.IntegerField(default=0)

    def __str__(self):
        return '{} reviewed {} (approved: {})'.format(self.user, self.issue, self.approved)


class Issue(models.Model):
    number = models.IntegerField(db_index=True)
    title = models.CharField(max_length=4096)
    points = models.IntegerField(null=True)
    repository = models.ForeignKey(Repository)
    state = models.SmallIntegerField(default=State.closed.value)
    milestone = models.ForeignKey(Milestone, null=True)

    labels = models.ManyToManyField(Label)

    committers = models.ManyToManyField(User, related_name='built_issues', through=CommitterM2M)
    reviewers = models.ManyToManyField(User, 'reviewed_issues', through=ReviewerM2M)
    lines_added = models.IntegerField(default=0)
    lines_removed = models.IntegerField(default=0)
    # project ???

    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()
    pr_opened_at = models.DateTimeField(null=True)
    closed_at = models.DateTimeField(null=True)

    def __str__(self):
        return '#{}: {}'.format(self.number, self.title)

    def get_points_from_title(self):
        match = POINT_PATTERN.search(self.title)
        if match:
            return int(match.groups(0)[0])
        return None

    def save(self, **kwargs):
        points = self.get_points_from_title()
        if points is not None:
            self.points = points
        super(Issue, self).save(**kwargs)

    @property
    def url(self):
        return 'https://github.com/7Geese/7Geese/issues/{}'.format(self.number)